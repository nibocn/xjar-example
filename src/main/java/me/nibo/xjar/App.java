package me.nibo.xjar;

import static io.xjar.XFilters.ant;
import static io.xjar.XFilters.not;

import org.apache.commons.compress.archivers.jar.JarArchiveEntry;

import io.xjar.XCryptos;
import io.xjar.XFilters;
import io.xjar.XKit;
import io.xjar.boot.XBoot;
import io.xjar.boot.XBootEncryptor;
import io.xjar.filter.XAllEntryFilter;
import io.xjar.filter.XMixEntryFilter;

public class App {
    public String getGreeting() {
        return "Hello world.";
    }

    public static void main(String[] args) throws Exception {
        System.out.println("开始加密 jar 包...");
        // XCryptos.encryption().from("/Users/richard/Workspace/IdeaProjects/xjar-example/spring-boot-xjar-0.0.1.jar")
        //     .use("abc123456")
        //     .exclude("BOOT-INF/classes/bootstrap.yml")
        //     .exclude("BOOT-INF/classes/license.lic")
        //     .exclude("BOOT-INF/classes/logback-spring.xml")
        //     .exclude("BOOT-INF/classes/static/**/*")
        //     .to("/Users/richard/Workspace/IdeaProjects/xjar-example/spring-boot-xjar-0.0.1-enc.jar");
        XAllEntryFilter<JarArchiveEntry> excludes = XKit.all();
        excludes.mix(not(ant("application.yml")));
        excludes.mix(not(ant("bootstrap.yml")));
        excludes.mix(not(ant("logback-spring.xml")));
        excludes.mix(not(ant("license.lic")));
        excludes.mix(not(ant("static/**/*")));
        XMixEntryFilter<JarArchiveEntry> filter = XKit.all();
        filter.mix(excludes);

        XBoot.encrypt(
            "/Users/richard/Workspace/IdeaProjects/xjar-example/auth-service-0.0.1-boot.jar",
            "/Users/richard/Workspace/IdeaProjects/xjar-example/auth-service-0.0.1-boot-enc.jar",
            XKit.key("ALUMxa9Gf7GWRKu7o72X"), filter
        );
        System.out.println("加密完成");
    }
}
